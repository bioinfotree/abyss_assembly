# Copyright 2013 Michele Vidotto <michele.vidotto@gmail.com>

# string naming the project. The same for all libraries
PRJ_NAME ?= test 
# kmer length
KMER ?= 60
# length for filtering contigs
FILTER_LENGTH ?= 500
# inizialize empty var

# a quoted list of whitespace-separated paired-end library names. Use this varible when assembling
# data from multiple paired-end libraries
# list of library_names
LIB		?=
# list of: library_name='files path'
LIB_GROUPS	?=

# list  of  paired-end  libraries  that will be used only for merging unitigs into contigs and
# will not contribute toward the consensus sequence.
# list of library_names
PE		?=
# list of: library_name='files path'
PE_GROUPS	?=

# list of mate-pair libraries that will be used for scaffolding.
# list of library_names
MP		?=
# list of: library_name='files path'
MP_GROUPS	?=

# list  of  long  sequence  libraries  that  will  be  used  for rescaffolding.
# list of library_names
LONG		?=
# list of: library_name='files path'
LONG_GROUPS	?=

# files containing single-end reads
# list of: file_paths
SE		?=



logs:
	mkdir -p $@




# abyss-pe is a makefile which is interpreted by a normal GNU make
# since all the dipendences are not correctly setted, it returns: "make[1]: *** read jobs pipe: Resource temporarily unavailable.  Stop."
# when the process find MAKEFLAGS="-j X", X>1 in the environ.
# In order to avoid this MAKEFLAGS environ varible is reset before being passed to abyss-pe.
define costant_part
	$(call load_modules); \
	export MAKEFLAGS=""; \   * reset MAKEFLAGS before pass it to abyss-pe which is a makefile *
	abyss-pe \
	k=$(KMER) \   * size of k-mer (bp) *
	verbose=-v \
	np=$$THREADNUM \
	$(call variable_part)   * part that should be present in the makefile *
endef


# execute the part of the pipeline than can be really parallelized
$(PRJ_NAME)-1.fa: logs
	!threads
	$(call costant_part) \
	$@ \
	2>&1 \
	| tee $</abyss-pe.$@.log   * [tool].[taget].log *



# Execute the part of the pipeline than is not parallelized.
# Moreover abyss-pe has SHELL=/bin/bash -o pipefail. Pipefail require that all commands of a pipe must succeed.
# For some reasons abyss-pe returns an error when is building the last targhet $(PRJ_NAME)-scaffolds.fa.
# The error is passed to the level [1] makefile so bmake attempt to delete the targhet. .IGNORE: avoid this!
.IGNORE: $(PRJ_NAME)-scaffolds.fa
$(PRJ_NAME)-scaffolds.fa: logs $(PRJ_NAME)-1.fa
	!threads
	$(call costant_part) \
	2>&1 \
	| tee $</abyss-pe.$@.log; \   * [tool].[taget].log *
	touch $@



$(PRJ_NAME)-contigs.fa: $(PRJ_NAME)-scaffolds.fa
	touch $@


$(FILTER_LENGTH)bp.$(PRJ_NAME)-%.fa: $(PRJ_NAME)-%.fa
	filter_contigs --fasta $< --length $(FILTER_LENGTH) >$@


$(FILTER_LENGTH)bp.$(PRJ_NAME)-%.gam: $(FILTER_LENGTH)bp.$(PRJ_NAME)-%.fa
	$(call load_modules); \
	gam-n50 $< \
	| tr \\t \\n >$@




.PHONY: test
test:
	@echo $(FRAGMENT_NAME1)


ALL +=  $(PRJ_NAME)-1.fa \
	$(PRJ_NAME)-scaffolds.fa \
	$(FILTER_LENGTH)bp.$(PRJ_NAME)-contigs.fa \
	$(FILTER_LENGTH)bp.$(PRJ_NAME)-scaffolds.fa \
	$(FILTER_LENGTH)bp.$(PRJ_NAME)-contigs.gam \
	$(FILTER_LENGTH)bp.$(PRJ_NAME)-scaffolds.gam


INTERMEDIATE +=

CLEAN += logs \
	 $(wildcard $(PRJ_NAME)*) \
	 $(wildcard *hist) \
	 $(wildcard *dist*)
